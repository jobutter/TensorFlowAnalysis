from sympy.physics.quantum.spin import Rotation
from sympy import pi, symbols, Rational

theta = symbols('theta')
#for j in range(1, 9, 2) : 
#  for m1 in [-1, 1] : 
#    for m2 in [-1, 1] : 
#      print "  if j == ", j, " and m1 == ", m1, " and m2 == ", m2, " : return ", Rotation.d( Rational(j, 2), Rational(m1, 2), Rational(m2, 2), theta).doit()

for j in range(0, 8, 2) : 
  for m1 in [-2, 0, 2] : 
    for m2 in [-2, 0, 2] : 
      print "  if j == ", j, " and m1 == ", m1, " and m2 == ", m2, " : return ", Rotation.d( Rational(j, 2), Rational(m1, 2), Rational(m2, 2), theta).doit()
